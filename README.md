Micro MQTT wire protocol modern C++ library

The main goals are:
+ minimal overhead of performance and memory use
+ modern C++ approach (C++20 at the moment)
+ MCU and embedded platforms friendly (in particular, allow non-STL data structures to be used)
+ maximum test coverage
+ MQTT 3.1 adoption
