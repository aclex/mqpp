/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/basic.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr size_t g_packet_length{160};
	constexpr size_t g_header_size{3};
	constexpr packet::type g_packet_type{packet::type::pingreq};

	class test_basic : public basic
	{
	public:
		test_basic(const packet::type t, const size_t remaining_length) noexcept :
			basic(t, remaining_length)
		{

		}

		test_basic(const packet::type t, const span<uint8_t> data, const size_t fixed_header_size) noexcept :
			basic(t, data, fixed_header_size)
		{
				
		}

		size_t data_start() const noexcept {return basic::data_start();}
		size_t remaining_length() const noexcept {return basic::remaining_length();}
	};

	bool check_placement_ctr(const test_basic& pkt) noexcept
	{
		return pkt.data_start() == 3;
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		if (data[0] != (static_cast<uint8_t>(g_packet_type) << 4) ||
			data[1] != 160 ||
			data[2] != 1)
			return false;

		return true;
	}

	bool check_remaining_length(const test_basic& pkt) noexcept
	{
		return pkt.remaining_length() == g_packet_length;
	}
}

int main(int, char**)
{
	vector<uint8_t> v(g_packet_length + g_header_size);
	iota(begin(v), end(v), -3);
	v[0] = (static_cast<uint8_t>(g_packet_type) << 4);
	v[1] = 128 + 32; // residue (160 - 128 = 32) and signal flag in MSB
	v[2] = 1; // 1 * 128

	test_basic pkt1{g_packet_type, v, g_header_size};

	if (!check_placement_ctr(pkt1))
		throw runtime_error("Placement constructor fails.");

	if (!check_remaining_length(pkt1))
		throw runtime_error("Remaining length fails.");

	test_basic pkt2{packet::type::pingreq, g_packet_length};
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
