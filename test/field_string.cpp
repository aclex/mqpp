/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>

#include <mqpp/packet/field/string.h>

using namespace std;

using namespace mqpp::packet;

namespace
{
	void test_load(field::string<uint8_t>& f)
	{
		const string_view s{f};
		if (s != "test")
			throw runtime_error("test_load fails.");
	}

	void test_upload(array<uint8_t, 10>& arr, field::string<uint8_t>& f)
	{
		f = "field";
		if (arr[0] != 0 || arr[1] != 5 ||
			arr[2] != 'f' || arr[3] != 'i' || arr[4] != 'e' || arr[5] != 'l' || arr[6] != 'd')
			throw runtime_error("test_upload fails.");
	}

	void test_size(field::string<uint8_t>& f)
	{
		if (f.size() != 6)
			throw runtime_error("test_size fails.");
	}

	void test_estimate()
	{
		const auto s{mqpp::packet::field::string<>::estimate("estimate")};

		if (s != 10)
			throw runtime_error("test_estimate fails.");
	}
}

int main(int, char**)
{
	array<uint8_t, 10> arr{0, 4, 't', 'e', 's', 't'};
	mqpp::packet::field::string f{span<uint8_t, dynamic_extent>{arr}};
	test_estimate();
	test_size(f);
	test_load(f);
	test_upload(arr, f);

	return 0;
}
