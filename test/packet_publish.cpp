/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/publish.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr packet::type g_packet_type{packet::type::publish};

	constexpr string_view topic{"Тема А"sv};

	bool check_header_fields(const publish& pkt) noexcept
	{
		if (!pkt.identifier() || pkt.identifier() != 0x2518)
			return false;

		if (pkt.topic() != topic)
			return false;

		if (pkt.qos() != qos::at_least_once)
			return false;

		if (!pkt.retain())
			return false;

		if (!pkt.duplicate())
			return false;

		return true;
	}

	bool check_placement_ctr_zero(const publish& pkt) noexcept
	{
		if (!check_header_fields(pkt))
			return false;

		if (pkt.payload().size() != 0)
			return false;

		return true;
	}

	bool check_placement_ctr(const publish& pkt) noexcept
	{
		if (!check_header_fields(pkt))
			return false;

		vector<uint8_t> expected(32);
		iota(begin(expected), end(expected), 0);

		const auto result{pkt.payload()};
		if (expected.size() != result.size())
			return false;

		return equal(begin(expected), end(expected), begin(result));
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		vector<uint8_t> expected(2);
		expected[0] = static_cast<uint8_t>((static_cast<uint8_t>(g_packet_type) << 4) | 0b1011);

		expected.push_back(0);
		expected.push_back(topic.size());
		expected.insert(end(expected), begin(topic), end(topic));

		expected.push_back(0);
		expected.push_back(0);

		vector<uint8_t> payload(32);
		iota(begin(payload), end(payload), 0);

		expected.insert(end(expected), begin(payload), end(payload));

		expected[1] = expected.size() - 2;

		if (data[1] != expected.size() - 2)
			return false;

		const auto result{data};

		if (expected.size() != result.size())
			return false;

		return equal(begin(expected), end(expected), begin(result));
	}
}

int main(int, char**)
{
	vector<uint8_t> v(2);
	v[0] = static_cast<uint8_t>((static_cast<uint8_t>(g_packet_type) << 4) | 0b1011);

	v.push_back(0);
	v.push_back(topic.size());
	v.insert(end(v), begin(topic), end(topic));

	v.push_back(0x25);
	v.push_back(0x18);

	v[1] = v.size() - 2;

	const auto zero_payload_pkt{basic::decode(v)};

	if (!check_placement_ctr_zero(*static_cast<publish*>(zero_payload_pkt.get())))
		throw runtime_error("Placement constructor for packet with zero payload fails.");

	vector<uint8_t> payload(32);
	iota(begin(payload), end(payload), 0);

	v.insert(end(v), begin(payload), end(payload));

	v[1] = v.size() - 2;

	const auto pkt1{basic::decode(v)};

	if (!check_placement_ctr(*static_cast<publish*>(pkt1.get())))
		throw runtime_error("Placement constructor fails.");

	publish pkt2{topic, span{payload}, qos::at_least_once, true};
	pkt2.mark_duplicate();
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
