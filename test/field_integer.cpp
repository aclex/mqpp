/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>

#include <mqpp/packet/field/integer.h>

using namespace std;

using namespace mqpp::packet;

namespace
{
	void test_load(field::integer<uint8_t>& f)
	{
		const uint16_t d{f};
		if (d != 5411)
			throw runtime_error("test_load fails.");
	}

	void test_upload(array<uint8_t, 2>& arr, field::integer<uint8_t>& f)
	{
		f = uint16_t{5634};
		if (arr[0] != 0x16 || arr[1] != 0x02)
			throw runtime_error("test_upload fails.");
	}

	void test_size(field::integer<uint8_t>& f)
	{
		if (f.size() != 2)
			throw runtime_error("test_size fails.");
	}

	void test_estimate()
	{
		const auto s{mqpp::packet::field::integer<>::estimate()};

		if (s != 2)
			throw runtime_error("test_estimate fails.");
	}
}

int main(int, char**)
{
	array<uint8_t, 2> arr{0x15, 0x23};
	mqpp::packet::field::integer f{span<uint8_t, dynamic_extent>{arr}};
	test_load(f);
	test_size(f);
	test_estimate();
	test_upload(arr, f);

	return 0;
}
