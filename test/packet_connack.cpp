/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/connack.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr size_t g_fixed_header_size{2};
	constexpr packet::type g_packet_type{packet::type::connack};
	constexpr connack::status g_response{connack::status::server_unavailable};
	constexpr bool g_session_present{true};

	bool check_placement_ctr(const connack& pkt) noexcept
	{
		if (pkt.response() != g_response)
			return false;

		if (pkt.session_present() != g_session_present)
			return false;
		
		return true;
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		if (data[2] != static_cast<uint8_t>(g_session_present) ||
			data[3] != static_cast<uint8_t>(g_response))
			return false;

		return true;
	}
}

int main(int, char**)
{
	vector<uint8_t> v(4);
	v[0] = (static_cast<uint8_t>(g_packet_type) << 4);
	v[1] = g_fixed_header_size;
	v[2] = static_cast<uint8_t>(g_session_present);
	v[3] = static_cast<uint8_t>(g_response);

	const auto pkt1{basic::decode(v)};

	if (!check_placement_ctr(*static_cast<connack*>(pkt1.get())))
		throw runtime_error("Placement constructor fails.");

	connack pkt2{g_response, g_session_present};
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
