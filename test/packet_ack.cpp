/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/ack.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr size_t g_response_length{3};
	constexpr packet::type g_packet_type{packet::type::suback};

	class test_ack : public ack
	{
	public:
		test_ack(const packet::type t, const uint16_t packet_identifier) noexcept :
			ack(t, packet_identifier)
		{

		}

		test_ack(const packet::type t, const span<uint8_t> data, const size_t fixed_header_size) noexcept :
			ack(t, data, fixed_header_size)
		{
				
		}

		size_t response_start() const noexcept {return ack::response_start();}

		span<uint8_t> data() const noexcept {return m_data;}
	};

	bool check_placement_ctr(const test_ack& pkt) noexcept
	{
		if (pkt.packet_identifier() != 0x2518)
			return false;

		if (pkt.data()[pkt.response_start()] != 1 ||
			pkt.data()[pkt.response_start() + 1] != 2 ||
			pkt.data()[pkt.response_start() + 2] != 3)
		{
			return false;
		}
		
		return true;
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		if (data.size() != 4)
			return false;

		if (data[2] != 0x25 ||
			data[3] != 0x18)
			return false;

		return true;
	}
}

int main(int, char**)
{
	vector<uint8_t> v(7);
	v[0] = (static_cast<uint8_t>(g_packet_type) << 4);
	v[1] = 5;
	v[2] = 0x25;
	v[3] = 0x18;
	v[4] = 1;
	v[5] = 2;
	v[6] = 3;

	test_ack pkt1{g_packet_type, v, 2};

	if (!check_placement_ctr(pkt1))
		throw runtime_error("Placement constructor fails.");

	test_ack pkt2{packet::type::suback, 0x2518};
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
