/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>

#include <mqpp/packet/field/payload.h>

using namespace std;

using namespace mqpp::packet;

namespace
{
	void test_load(field::payload<uint8_t>& f)
	{
		const span<uint8_t> s{f};
		if (s.size() != 16 ||
			s[0] != 0x12 ||
			s[1] != 0x34 ||
			s[2] != 0x56 ||
			s[3] != 0x78 ||
			s[4] != 0x9a ||
			s[5] != 0xbc ||
			s[6] != 0xde ||
			s[7] != 0xf0)
			throw runtime_error("test_load fails.");
	}

	void test_upload(array<uint8_t, 16>& arr, field::payload<uint8_t>& f)
	{
		array<uint8_t, 10> n
		{
			0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9
		};

		f = span<uint8_t, dynamic_extent>{n};
		if (!equal(begin(n), end(n), begin(arr)))
			throw runtime_error("test_upload fails.");
	}

	void test_size(field::payload<uint8_t>& f)
	{
		if (f.size() != 16)
			throw runtime_error("test_size fails.");
	}

	void test_estimate(array<uint8_t, 16>& arr)
	{
		const auto s{mqpp::packet::field::payload<>::estimate(span<uint8_t, dynamic_extent>{arr})};

		if (s != 16)
			throw runtime_error("test_estimate fails.");
	}
}

int main(int, char**)
{
	array<uint8_t, 16> arr{0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0};
	test_estimate(arr);
	mqpp::packet::field::payload f{span<uint8_t, dynamic_extent>{arr}};
	test_size(f);
	test_load(f);
	test_upload(arr, f);

	return 0;
}
