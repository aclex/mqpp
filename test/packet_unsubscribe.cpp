/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/unsubscribe.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr packet::type g_packet_type{packet::type::unsubscribe};

	constexpr string_view topic_a{"Тема А"sv};
	constexpr string_view topic_b{"Тема БВГ"sv};

	bool check_placement_ctr(const unsubscribe& pkt) noexcept
	{
		const vector expected
		{
			topic_a,
			topic_b
		};

		const auto& result{pkt.topics()};

		if (expected.size() != result.size())
			return false;

		return equal(begin(expected), end(expected), begin(result));
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		vector<uint8_t> expected(2);
		expected[0] = 0;
		expected[1] = 0;

		expected.push_back(0);
		expected.push_back(topic_a.size());
		expected.insert(end(expected), begin(topic_a), end(topic_a));

		expected.push_back(0);
		expected.push_back(topic_b.size());
		expected.insert(end(expected), begin(topic_b), end(topic_b));

		if (data[1] != expected.size())
			return false;

		const auto result{data.subspan(2)};

		if (expected.size() != result.size())
			return false;

		return equal(begin(expected), end(expected), begin(result));
	}
}

int main(int, char**)
{
	vector<uint8_t> v(4);
	v[0] = (static_cast<uint8_t>(g_packet_type) << 4);
	v[2] = 0x25;
	v[3] = 0x18;

	v.push_back(0);
	v.push_back(topic_a.size());
	v.insert(end(v), begin(topic_a), end(topic_a));

	v.push_back(0);
	v.push_back(topic_b.size());
	v.insert(end(v), begin(topic_b), end(topic_b));

	v[1] = v.size() - 2;

	const auto pkt1{basic::decode(v)};

	if (!check_placement_ctr(*static_cast<unsubscribe*>(pkt1.get())))
		throw runtime_error("Placement constructor fails.");

	unsubscribe pkt2{topic_a, topic_b};
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
