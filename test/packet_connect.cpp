/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include <iostream>
#include <numeric>

#include <mqpp/packet/connect.h>

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;

namespace
{
	constexpr packet::type g_packet_type{packet::type::connect};

	constexpr string_view g_client_id{"Client3"sv};
	constexpr string_view g_will_topic{"Тема А"sv};

	constexpr string_view g_username{"username"sv};
	constexpr string_view g_password{"password"sv};

	constexpr auto g_keep_alive{60s};

	vector<uint8_t> create_encoded_packet()
	{
		vector<uint8_t> v(12);
		v[0] = (static_cast<uint8_t>(g_packet_type) << 4);
		v[2] = 0;
		v[3] = 4;
		v[4] = 'M';
		v[5] = 'Q';
		v[6] = 'T';
		v[7] = 'T';
		v[8] = 0b100;
		v[9] = 0b11100110;
		v[10] = 0;
		v[11] = 60;

		v.push_back(0);
		v.push_back(g_client_id.size());
		v.insert(end(v), begin(g_client_id), end(g_client_id));

		v.push_back(0);
		v.push_back(g_will_topic.size());
		v.insert(end(v), begin(g_will_topic), end(g_will_topic));

		vector<uint8_t> will_payload(32);
		iota(begin(will_payload), end(will_payload), 0);

		v.push_back(0);
		v.push_back(will_payload.size());
		v.insert(end(v), begin(will_payload), end(will_payload));

		v.push_back(0);
		v.push_back(g_username.size());
		v.insert(end(v), begin(g_username), end(g_username));

		v.push_back(0);
		v.push_back(g_password.size());
		v.insert(end(v), begin(g_password), end(g_password));

		v[1] = v.size() - 2;

		return v;
	}

	publish create_will_packet()
	{
		vector<uint8_t> payload(32);
		iota(begin(payload), end(payload), 0);

		return {g_will_topic, span{payload}, qos::at_most_once, true};
	}

	bool check_placement_ctr(const connect& pkt) noexcept
	{
		if (pkt.client_id() != g_client_id)
			return false;

		if (!pkt.username() || *pkt.username() != g_username)
			return false;

		if (!pkt.password() || *pkt.password() != g_password)
			return false;

		if (!pkt.clean_session())
			return false;

		if (pkt.keep_alive() != g_keep_alive)
			return false;

		const auto& expected_will_pkt{create_will_packet()};

		if (!pkt.will())
			return false;

		const auto will_packet_ptr{pkt.will()};
		const auto& result_will_pkt{*will_packet_ptr};

		if (result_will_pkt.topic() != expected_will_pkt.topic())
			return false;

		if (result_will_pkt.payload().size() != expected_will_pkt.payload().size())
			return false;

		return equal(begin(result_will_pkt.payload()), end(result_will_pkt.payload()), begin(expected_will_pkt.payload()));
	}

	bool check_gen_ctr(const span<const uint8_t> data) noexcept
	{
		auto expected(create_encoded_packet());

		const auto result{data};

		if (expected.size() != result.size())
			return false;

		return equal(begin(expected), end(expected), begin(result));
	}
}

int main(int, char**)
{
	auto data{create_encoded_packet()};
	const auto pkt1{basic::decode(data)};

	if (!check_placement_ctr(*static_cast<connect*>(pkt1.get())))
		throw runtime_error("Placement constructor fails.");

	const auto& will_pkt{create_will_packet()};
	connect pkt2{g_client_id, g_username, g_password, true, g_keep_alive, &will_pkt};
	const auto encoded{pkt2.encode()};

	if (!check_gen_ctr(encoded))
		throw runtime_error("Generating constructor fails.");

	return 0;
}
