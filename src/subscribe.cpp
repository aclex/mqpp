/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/subscribe.h"

#include <numeric>

#include "mqpp/util.h"

#include "mqpp/packet/identifier.h"

#include "mqpp/packet/field/integer.h"
#include "mqpp/packet/field/payload.h"
#include "mqpp/packet/field/string.h"

using namespace std;

using namespace mqpp;
using namespace mqpp::packet;
using namespace mqpp::util;

subscribe::subscribe(const initializer_list<subscription> subscriptions) noexcept :
	basic(type::subscribe, estimate(subscriptions)),
	m_subscriptions_start(data_start() + 2)
{
	size_t counter{data_start()};

	m_data[0] |= 2;

	field::integer pi_field{m_data.subspan(counter)};
	pi_field = generate_identifier();
	counter += pi_field.size();

	for (const auto& subscription : subscriptions)
	{
		field::string st_field{m_data.subspan(counter)};
		st_field = subscription.topic;
		counter += st_field.size();

		m_data[counter] = static_cast<uint8_t>(subscription.qos);
		++counter;
	}
}

subscribe::subscribe(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(type::subscribe, data, fixed_header_size),
	m_subscriptions_start(data_start() + 2)
{

}

vector<subscription> subscribe::subscriptions() const
{
	vector<subscription> result;

	for (size_t counter = m_subscriptions_start; counter < m_data.size();)
	{
		field::string st_field{m_data.subspan(counter)};
		counter += st_field.size();

		result.emplace_back(st_field, mqpp::qos{m_data[counter]});
		++counter;
	}

	return result;
}

size_t subscribe::estimate(const initializer_list<subscription> subscriptions) noexcept
{
	return accumulate(begin(subscriptions), end(subscriptions), size_t{2}, [](const size_t a, const subscription& b){return a + field::string<>::estimate(b.topic) + 1;});
}
