/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/ack.h"

#include "mqpp/util.h"

#include "mqpp/packet/field/integer.h"

using namespace std;

using namespace mqpp::util;
using namespace mqpp::packet;

ack::ack(const packet::type t, const uint16_t packet_identifier, const size_t response_length) noexcept :
	basic(t, estimate(response_length))
{
	size_t counter{data_start()};

	field::integer pi_field{m_data.subspan(counter)};
	pi_field = packet_identifier;

	counter += pi_field.size();
}

ack::ack(const packet::type t, const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(t, data, fixed_header_size)
{

}

uint16_t ack::packet_identifier() const noexcept
{
	field::integer pi_field{m_data.subspan(data_start())};
	return static_cast<uint16_t>(pi_field);
}

size_t ack::estimate(const size_t response_length) noexcept
{
	return s_variable_header_size + response_length;
}

size_t ack::response_start() const noexcept
{
	return data_start() + s_variable_header_size;
}
