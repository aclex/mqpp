/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/connect.h"

#include "mqpp/util.h"

#include "mqpp/packet/field/integer.h"
#include "mqpp/packet/field/payload.h"
#include "mqpp/packet/field/string.h"

using namespace std;
using namespace std::chrono;

using namespace mqpp::util;
using namespace mqpp::packet;

connect::connect(const string_view client_id, const optional<string_view>& username, const optional<string_view> password, const bool clear_session, const seconds keep_alive, const publish* const will) noexcept :
	basic(type::connect, estimate(client_id, username, password, will)),
	m_connect_flags_start(data_start() + 7),
	m_keep_alive_start(data_start() + 8),
	m_client_id_start(data_start() + 10)
{
	size_t counter{data_start()};

	field::string protocol{m_data.subspan(counter)};
	protocol = "MQTT";
	counter += protocol.size();

	m_data[counter++] = s_protocol_version;
	m_data[counter++] = collect_flags(username, password, will, clear_session);

	field::integer ka_field{m_data.subspan(counter)};
	ka_field = static_cast<uint16_t>(keep_alive.count());
	counter += ka_field.size();

	field::string client_id_field{m_data.subspan(counter)};
	client_id_field = client_id;
	counter += client_id_field.size();

	m_will_start = counter;

	if (will)
	{
		field::string will_topic{m_data.subspan(counter)};
		will_topic = will->topic();
		counter += will_topic.size();

		field::integer will_message_length_field{m_data.subspan(counter)};
		will_message_length_field = static_cast<uint16_t>(will->payload().size());
		counter += will_message_length_field.size();

		field::payload will_message{m_data.subspan(counter, will->payload().size())};
		will_message = will->payload();
		counter += will_message.size();
	}

	m_username_start = counter;
	m_password_start = counter;

	if (username)
	{
		field::string un_field{m_data.subspan(counter)};
		un_field = username.value();
		counter += un_field.size();

		m_password_start = counter;
		if (password)
		{
			field::string pw_field{m_data.subspan(counter)};
			pw_field = password.value();
			counter += pw_field.size();
		}
	}
}

connect::connect(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(type::connect, data, fixed_header_size),
	m_connect_flags_start(data_start() + 7),
	m_keep_alive_start(data_start() + 8),
	m_client_id_start(data_start() + 10)
{
	size_t counter{m_client_id_start};
	field::string client_id_field{m_data.subspan(counter)};
	counter += client_id_field.size();

	m_will_start = counter;

	if (has_will())
	{
		field::string will_topic{m_data.subspan(counter)};
		counter += will_topic.size();

		field::integer will_message_length{m_data.subspan(counter)};
		counter += will_message_length + will_message_length.size();
	}

	m_username_start = counter;
	m_password_start = counter;

	if (has_username())
	{
		field::string un_field{m_data.subspan(counter)};
		counter += un_field.size();

		m_password_start = counter;
		if (has_password())
		{
			field::string pw_field{m_data.subspan(counter)};
			counter += pw_field.size();
		}
	}
}

string_view connect::client_id() const noexcept
{
	field::string cid_field{m_data.subspan(m_client_id_start)};
	return cid_field;
}

optional<string_view> connect::username() const noexcept
{
	if (!has_username())
		return {};

	field::string u_field{m_data.subspan(m_username_start)};
	return u_field;
}

optional<string_view> connect::password() const noexcept
{
	if (!has_username() || !has_password())
		return {};

	field::string p_field{m_data.subspan(m_password_start)};
	return p_field;
}

bool connect::clean_session() const noexcept
{
	return m_data[m_connect_flags_start] & (1 << 1);
}

seconds connect::keep_alive() const noexcept
{
	field::integer ka_field{m_data.subspan(m_keep_alive_start)};
	return seconds{static_cast<uint16_t>(ka_field)};
}

unique_ptr<publish> connect::will() const noexcept
{
	if (!has_will())
		return {};

	size_t counter{m_will_start};
	field::string will_topic{m_data.subspan(counter)};
	counter += will_topic.size();

	field::integer will_message_length_field{m_data.subspan(counter)};
	counter += will_message_length_field.size();

	const uint16_t will_message_length{will_message_length_field};

	const auto will_message{m_data.subspan(counter, will_message_length)};
	counter += will_message.size();

	const mqpp::qos will_qos{static_cast<uint8_t>((m_data[m_connect_flags_start] & 0x18) >> 3)};
	const bool will_retain{static_cast<bool>(m_data[m_connect_flags_start] & 0x20)};

	const string_view temp{will_topic};
	return make_unique<publish>(temp, will_message, will_qos, will_retain);
}

size_t connect::estimate(const string_view client_id, const optional<string_view>& username, const optional<string_view>& password, const publish* const will) noexcept
{
	return 10 + field::string<>::estimate(client_id) + 
		(username.has_value() ? field::string<>::estimate(username.value()) : 0) +
		(password.has_value() ? field::string<>::estimate(password.value()) : 0) +
		(will ? (field::string<>::estimate(will->topic()) + 2 + field::payload<>::estimate(will->payload())) : 0);
}

uint8_t connect::collect_flags(const optional<string_view>& username, const optional<string_view>& password, const publish* const will, bool clear_session) noexcept
{
	return static_cast<uint8_t>((username.has_value() ? 1 << 7 : 0) |
		(password.has_value() ? 1 << 6 : 0) |
		(will ? (static_cast<uint8_t>(will->retain()) << 5) : 0) |
		(will ? (static_cast<uint8_t>(will->qos()) << 3) : 0) |
		(will ? 1 << 2 : 0) |
		(static_cast<uint8_t>(clear_session) << 1));
}

bool connect::has_will() const noexcept
{
	return m_data[m_connect_flags_start] & (1 << 2);
}

bool connect::has_username() const noexcept
{
	return m_data[m_connect_flags_start] & (1 << 7);
}

bool connect::has_password() const noexcept
{
	return m_data[m_connect_flags_start] & (1 << 6);
}
