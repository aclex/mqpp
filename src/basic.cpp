/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/basic.h"

#include "mqpp/packet/connect.h"
#include "mqpp/packet/connack.h"
#include "mqpp/packet/publish.h"
#include "mqpp/packet/puback.h"
#include "mqpp/packet/pubrec.h"
#include "mqpp/packet/pubrel.h"
#include "mqpp/packet/pubcomp.h"
#include "mqpp/packet/subscribe.h"
#include "mqpp/packet/suback.h"
#include "mqpp/packet/unsubscribe.h"
#include "mqpp/packet/unsuback.h"
#include "mqpp/packet/pingreq.h"
#include "mqpp/packet/pingresp.h"
#include "mqpp/packet/disconnect.h"

#include "mqpp/packet/type.h"

using namespace std;

using namespace mqpp::packet;

basic::basic(const packet::type pt, const size_t remaining_length) noexcept :
	m_buffer(full_length(remaining_length)),
	m_data(m_buffer),
	m_data_start(1 + remaining_length_width(remaining_length)),
	m_type(pt)
{
	place_header();
}

basic::basic(const packet::type pt, const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	m_data(data),
	m_data_start(fixed_header_size),
	m_type(pt)
{

}

unique_ptr<basic> basic::decode(const span<uint8_t> data) noexcept
{
	if (data.empty())
		return {};

	const packet::type pt{parse_packet_type(data)};

	const auto [remaining_length_size, remaining_length]{parse_remaining_length(data.subspan(1))};
	const auto fixed_header_size{1 + remaining_length_size};
	const auto ps{fixed_header_size + remaining_length};

	switch (pt)
	{
	case type::connect:
		return unique_ptr<connect>{new connect(data.subspan(0, ps), fixed_header_size)};

	case type::connack:
		return unique_ptr<basic>{new connack(data.subspan(0, ps), fixed_header_size)};

	case type::publish:
		return unique_ptr<publish>{new publish(data.subspan(0, ps), fixed_header_size)};

	case type::puback:
		return unique_ptr<puback>{new puback(data.subspan(0, ps), fixed_header_size)};

	case type::pubrec:
		return unique_ptr<pubrec>{new pubrec(data.subspan(0, ps), fixed_header_size)};

	case type::pubrel:
		return unique_ptr<pubrel>{new pubrel(data.subspan(0, ps), fixed_header_size)};

	case type::pubcomp:
		return unique_ptr<pubcomp>{new pubcomp(data.subspan(0, ps), fixed_header_size)};

	case type::subscribe:
		return unique_ptr<subscribe>{new subscribe(data.subspan(0, ps), fixed_header_size)};

	case type::suback:
		return unique_ptr<suback>{new suback(data.subspan(0, ps), fixed_header_size)};

	case type::unsubscribe:
		return unique_ptr<unsubscribe>{new unsubscribe(data.subspan(0, ps), fixed_header_size)};

	case type::unsuback:
		return unique_ptr<unsuback>{new unsuback(data.subspan(0, ps), fixed_header_size)};

	case type::pingreq:
		return unique_ptr<pingreq>{new pingreq(data.subspan(0, ps), fixed_header_size)};

	case type::pingresp:
		return unique_ptr<pingresp>{new pingresp(data.subspan(0, ps), fixed_header_size)};

	case type::disconnect:
		return unique_ptr<disconnect>{new disconnect(data.subspan(0, ps), fixed_header_size)};

	default:
		return {};
	}
}

span<const uint8_t, dynamic_extent> basic::encode() const noexcept
{
	return span{m_data};
}

size_t basic::data_start() const noexcept
{
	return m_data_start;
}

size_t basic::remaining_length() const noexcept
{
	return m_data.size() - m_data_start;
}

void basic::place_header() noexcept
{
	size_t counter{};

	m_data[0] = static_cast<uint8_t>(static_cast<uint8_t>(m_type) << 4);
	++counter;

	size_t s{remaining_length()};
	do
	{
		m_data[counter] = s % 128;
		s /= 128;

		if (s)
			m_data[counter] |= 128;

		++counter;
	}
	while(s);
}

size_t basic::remaining_length_width(const size_t remaining_length) noexcept
{
	if (remaining_length < 128) // = 2 ^ 7
		return 1;

	if (remaining_length < 16384) // = 2 ^ 14
		return 2;

	if (remaining_length < 2097152) // = 2 ^ 21
		return 3;

	return 4;
}

size_t basic::full_length(const size_t remaining_length) noexcept
{
	return 1 + remaining_length_width(remaining_length) + remaining_length;
}

size_t basic::find_data_start(const span<uint8_t> data) noexcept
{
	for (size_t i = 1; i < data.size(); ++i)
		if (!(data[i] & 128))
			return i + 1;

	return data.size();
}

mqpp::packet::type basic::parse_packet_type(const span<uint8_t> data) noexcept
{
	return mqpp::packet::type{static_cast<uint8_t>(data[0] >> 4)};
}

pair<size_t, size_t> basic::parse_remaining_length(const span<uint8_t> data) noexcept
{
	size_t field_width{};
	size_t value{};

	size_t multiplier{1};
	for (size_t i = 0; i < 4; ++i)
	{
		value += (data[i] & 0x7f) * multiplier;
		multiplier *= 128;

		if (!(data[i] & 128))
		{
			field_width = i + 1;
			break;
		}
	}

	return make_pair(field_width, value);
}
