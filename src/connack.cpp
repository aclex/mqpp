/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/connack.h"

#include "mqpp/util.h"

using namespace std;

using namespace mqpp::util;
using namespace mqpp::packet;

connack::connack(const status r, const bool session_present) noexcept :
	ack(type::connack, create_packet_identifier(r, session_present))
{

}

connack::connack(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	ack(type::connack, data, fixed_header_size)
{

}

connack::status connack::response() const noexcept
{
	return status{m_data[data_start() + 1]};
}

bool connack::session_present() const noexcept
{
	return static_cast<bool>(m_data[data_start()] & 0b1);
}

uint16_t connack::create_packet_identifier(const status r, const bool session_present) noexcept
{
	return static_cast<uint16_t>((static_cast<int>(session_present) << 8) | static_cast<uint8_t>(r));
}
