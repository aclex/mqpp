/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/publish.h"

#include "mqpp/util.h"

#include "mqpp/packet/identifier.h"

#include "mqpp/packet/field/integer.h"
#include "mqpp/packet/field/payload.h"
#include "mqpp/packet/field/string.h"

using namespace std;

using namespace mqpp::util;
using namespace mqpp::packet;

publish::publish(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(type::publish, data, fixed_header_size)
{
	size_t counter{data_start()};

	field::string t_field{m_data.subspan(counter)};
	counter += t_field.size();
	m_id_start = counter;

	if (this->qos() != qos::at_most_once)
	{
		field::integer pi_field{m_data.subspan(counter)};
		counter += pi_field.size();
	}

	m_payload_start = counter;
}

optional<uint16_t> publish::identifier() const noexcept
{
	if (qos() == mqpp::qos::at_most_once)
		return {};

	field::integer pi{m_data.subspan(m_id_start)};

	return static_cast<uint16_t>(pi);
}

string_view publish::topic() const noexcept
{
	field::string t{m_data.subspan(data_start())};
	return t;
}

span<uint8_t> publish::payload() const noexcept
{
	field::payload p{m_data.subspan(m_payload_start)};

	return p;
}

mqpp::qos publish::qos() const noexcept
{
	return mqpp::qos{static_cast<uint8_t>((m_data[0] & 0b110) >> 1)};
}

bool publish::retain() const noexcept
{
	return m_data[0] & 1;
}

bool publish::duplicate() const noexcept
{
	return m_data[0] & static_cast<uint8_t>(1 << 3);
}

void publish::mark_duplicate() noexcept
{
	m_data[0] |= static_cast<uint8_t>(1 << 3);
}
