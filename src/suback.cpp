/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/suback.h"

#include "mqpp/util.h"

#include "mqpp/packet/field/integer.h"

using namespace std;

using namespace mqpp::packet;

suback::suback(const uint16_t packet_identifier, const initializer_list<mqpp::qos> responses) noexcept :
	ack(type::suback, packet_identifier, estimate(responses))
{
	size_t counter{response_start()};

	transform(begin(responses), end(responses), begin(m_data) + counter, [](const mqpp::qos q){return static_cast<uint8_t>(q);});
}

suback::suback(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	ack(type::suback, data, fixed_header_size)
{

}

vector<mqpp::qos> suback::responses() const noexcept
{
	const auto r_field{m_data.subspan(response_start())};
	vector<mqpp::qos> result(r_field.size());
	transform(begin(r_field), end(r_field), begin(result), [](const uint8_t v){return mqpp::qos{v};});
	
	return result;
}

size_t suback::estimate(const initializer_list<mqpp::qos> responses) noexcept
{
	return responses.size();
}
