/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/unsubscribe.h"

#include <numeric>

#include "mqpp/util.h"

#include "mqpp/packet/identifier.h"

#include "mqpp/packet/field/integer.h"
#include "mqpp/packet/field/string.h"

using namespace std;

using namespace mqpp::util;
using namespace mqpp::packet;

unsubscribe::unsubscribe(const initializer_list<string_view> topics) noexcept :
	basic(type::unsubscribe, estimate(topics)),
	m_topics_start(data_start() + 2)
{
	size_t counter{data_start()};

	m_data[0] |= 2;

	field::integer pi_field{m_data.subspan(counter)};
	pi_field = generate_identifier();
	counter += pi_field.size();

	for (const auto& topic : topics)
	{
		field::string t_field{m_data.subspan(counter)};
		t_field = topic;
		counter += t_field.size();
	}
}

unsubscribe::unsubscribe(const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(type::unsubscribe, data, fixed_header_size),
	m_topics_start(data_start() + 2)
{

}

vector<string_view> unsubscribe::topics() const
{
	vector<string_view> result;

	for (size_t counter = m_topics_start; counter < m_data.size();)
	{
		field::string t_field{m_data.subspan(counter)};
		counter += t_field.size();

		result.emplace_back(t_field);
	}

	return result;
}

size_t unsubscribe::estimate(const initializer_list<string_view> topics) noexcept
{
	return accumulate(begin(topics), end(topics), size_t{2}, [](const size_t a, const string_view& b){return a + field::string<>::estimate(b);});
}
