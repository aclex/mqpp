/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#include "mqpp/packet/fixed.h"

#include "mqpp/util.h"

#include "mqpp/packet/field/integer.h"

using namespace std;

using namespace mqpp::util;
using namespace mqpp::packet;

fixed::fixed(const packet::type t) noexcept :
	basic(t, estimate())
{

}

fixed::fixed(const packet::type t, const span<uint8_t> data, const size_t fixed_header_size) noexcept :
	basic(t, data, fixed_header_size)
{

}

size_t fixed::estimate() noexcept
{
	return 0;
}
