/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_UTIL_H
#define MQPP_UTIL_H

#include <span>
#include <cstddef>

namespace mqpp::util
{
	// template<class Container> auto make_span(Container& c, const std::size_t counter) noexcept
	// {
		// return std::span{begin(c) + counter, c.size() - counter};
	// }
}

#endif // MQPP_UTIL_H
