/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_BASIC_H
#define MQPP_PACKET_BASIC_H

#include <memory>
#include <vector>
#include <span>
#include <utility>
#include <cstdint>
#include <cstddef>

#include <mqpp/packet/type.h>

namespace mqpp::packet
{
	class basic
	{
	public:
		static std::unique_ptr<basic> decode(const std::span<std::uint8_t> data) noexcept;
		std::span<const std::uint8_t> encode() const noexcept;
		packet::type type() const noexcept {return m_type;}
		virtual ~basic() = default;

	protected:
		explicit basic(const packet::type pt, const std::size_t remaining_length) noexcept;
		explicit basic(const packet::type pt, const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;

		basic(const basic&) = delete;
		basic(basic&&) = delete;
		basic& operator=(const basic&) = delete;
		basic& operator=(basic&&) = delete;

		std::size_t data_start() const noexcept;
		std::size_t remaining_length() const noexcept;

	private:
		void place_header() noexcept;
		static std::size_t remaining_length_width(const std::size_t remaining_length) noexcept;
		static std::size_t full_length(const std::size_t remaining_length) noexcept;
		static std::size_t find_data_start(const std::span<std::uint8_t> data) noexcept;
		static packet::type parse_packet_type(const std::span<std::uint8_t> data) noexcept;
		static std::pair<std::size_t, std::size_t> parse_remaining_length(const std::span<std::uint8_t> data) noexcept;

		std::vector<std::uint8_t> m_buffer;

	protected:
		std::span<std::uint8_t> m_data;

	private:
		const std::size_t m_data_start;
		const packet::type m_type;
	};
}

#endif // MQPP_PACKET_BASIC_H
