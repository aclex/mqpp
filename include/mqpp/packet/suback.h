/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef MQPP_PACKET_SUBACK_H
#define MQPP_PACKET_SUBACK_H

#include <initializer_list>

#include <mqpp/qos.h>

#include <mqpp/packet/ack.h>

namespace mqpp::packet
{
	class suback : public ack
	{
	public:
		explicit suback(const std::uint16_t identifier, const std::initializer_list<mqpp::qos> responses) noexcept;
		std::vector<mqpp::qos> responses() const noexcept;

	private:
		friend std::unique_ptr<basic> basic::decode(const std::span<std::uint8_t>) noexcept;

		explicit suback(const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;
		static std::size_t estimate(const std::initializer_list<mqpp::qos> responses) noexcept;
	};
}

#endif // MQPP_PACKET_SUBACK_H
