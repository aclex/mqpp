/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef MQPP_PACKET_CONNACK_H
#define MQPP_PACKET_CONNACK_H

#include <mqpp/packet/ack.h>

namespace mqpp::packet
{
	class connack : public ack
	{
	public:
		enum class status : unsigned char
		{
			accepted,
			unacceptance_protocol,
			identifier_rejected,
			server_unavailable,
			bad_username_or_password,
			not_authorized
		};

		connack(const status r, const bool session_present) noexcept;
		status response() const noexcept;
		bool session_present() const noexcept;

	private:
		friend std::unique_ptr<basic> basic::decode(const std::span<std::uint8_t>) noexcept;

		explicit connack(const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;
		static std::uint16_t create_packet_identifier(const status r, const bool session_present) noexcept;

		std::uint16_t packet_identifier() = delete;
	};
}

#endif // MQPP_PACKET_CONNACK_H
