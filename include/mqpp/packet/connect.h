/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_CONNECT_H
#define MQPP_PACKET_CONNECT_H

#include <chrono>
#include <optional>
#include <string_view>
#include <cstddef>
#include <cstdint>

#include <mqpp/packet/basic.h>
#include <mqpp/packet/publish.h>

namespace mqpp::packet
{
	class connect : public basic
	{
	public:
		connect(const std::string_view client_id, const std::optional<std::string_view>& username = {}, const std::optional<std::string_view> password = {}, const bool clean_session = true, const std::chrono::seconds keep_alive = std::chrono::seconds(0), const publish* const will = {}) noexcept;

		std::string_view client_id() const noexcept;
		std::optional<std::string_view> username() const noexcept;
		std::optional<std::string_view> password() const noexcept;
		bool clean_session() const noexcept;
		std::chrono::seconds keep_alive() const noexcept;
		std::unique_ptr<packet::publish> will() const noexcept;

	private:
		friend std::unique_ptr<basic> basic::decode(const std::span<std::uint8_t>) noexcept;

		explicit connect(const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;
		static std::size_t estimate(const std::string_view client_id, const std::optional<std::string_view>& username, const std::optional<std::string_view>& password, const publish* const will) noexcept;
		std::uint8_t collect_flags(const std::optional<std::string_view>& username, const std::optional<std::string_view>& password, const publish* const will, bool clear_session) noexcept;
		bool has_will() const noexcept;
		bool has_username() const noexcept;
		bool has_password() const noexcept;

		const std::size_t m_connect_flags_start;
		const std::size_t m_keep_alive_start;
		const std::size_t m_client_id_start;
		std::size_t m_will_start;
		std::size_t m_username_start;
		std::size_t m_password_start;

		static constexpr inline unsigned char s_protocol_version{0x04};
	};
}

#endif // MQPP_PACKET_CONNECT_H
