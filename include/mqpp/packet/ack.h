/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_ACK_H
#define MQPP_PACKET_ACK_H

#include <cstdint>
#include <cstddef>

#include <mqpp/packet/basic.h>

namespace mqpp::packet
{
	class ack : public basic
	{
	public:
		std::uint16_t packet_identifier() const noexcept;

	protected:
		ack(const packet::type t, const std::uint16_t packet_identifier, const std::size_t response_length = 0) noexcept;
		ack(const packet::type t, const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;
		std::size_t response_start() const noexcept;

	private:
		static std::size_t estimate(const std::size_t response_length) noexcept;

		static constexpr std::size_t s_variable_header_size{2};
	};
}

#endif // MQPP_PACKET_ACK_H
