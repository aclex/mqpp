/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef MQPP_PACKET_TYPE_H
#define MQPP_PACKET_TYPE_H

namespace mqpp::packet
{
	enum class type : unsigned char
	{
		connect = 1,
		connack = 2,
		publish = 3,
		puback = 4,
		pubrec = 5,
		pubrel = 6,
		pubcomp = 7,
		subscribe = 8,
		suback = 9,
		unsubscribe = 10,
		unsuback = 11,
		pingreq = 12,
		pingresp = 13,
		disconnect = 14
	};
}

#endif // MQPP_PACKET_TYPE_H
