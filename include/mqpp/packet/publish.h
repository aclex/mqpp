/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_PUBLISH_H
#define MQPP_PACKET_PUBLISH_H

#include <optional>
#include <string_view>

#include <mqpp/qos.h>

#include <mqpp/byte_wide.h>
#include <mqpp/packet/basic.h>

#include <mqpp/packet/field/integer.h>
#include <mqpp/packet/field/payload.h>
#include <mqpp/packet/field/string.h>
#include <mqpp/packet/identifier.h>

namespace mqpp::packet
{
	class publish : public basic
	{
	public:
		publish(const std::string_view topic, const std::span<byte_wide auto> payload = {}, const mqpp::qos q = mqpp::qos::at_most_once, const bool retain = false) noexcept :
			basic(type::publish, estimate(topic, payload, q))
		{
			using namespace std;

			using namespace mqpp::packet;

			size_t counter{data_start()};

			m_data[0] |= (static_cast<uint8_t>(q) << 1) | static_cast<uint8_t>(retain);

			field::string t_field{m_data.subspan(counter)};
			t_field = topic;
			counter += t_field.size();
			m_id_start = counter;

			if (q != qos::at_most_once)
			{
				field::integer pi_field{m_data.subspan(counter)};
				pi_field = generate_identifier();
				counter += pi_field.size();
			}

			m_payload_start = counter;

			field::payload p_field{m_data.subspan(m_payload_start)};
			p_field = payload;
			counter += p_field.size();
		}

		std::optional<std::uint16_t> identifier() const noexcept;
		std::string_view topic() const noexcept;
		std::span<std::uint8_t> payload() const noexcept;
		mqpp::qos qos() const noexcept;
		bool retain() const noexcept;
		bool duplicate() const noexcept;

		void mark_duplicate() noexcept;

	private:
		friend std::unique_ptr<basic> basic::decode(const std::span<std::uint8_t>) noexcept;

		explicit publish(const std::span<std::uint8_t> data, const std::size_t fixed_header_size) noexcept;
		static std::size_t estimate(const std::string_view topic, const std::span<byte_wide auto> payload, const mqpp::qos q) noexcept
		{
			return field::string<>::estimate(topic) +
				field::payload<>::estimate(payload) +
				(q != qos::at_most_once ? 2 : 0);
		}

		std::size_t m_id_start;
		std::size_t m_payload_start;
	};
}

#endif // MQPP_PACKET_PUBLISH_H
