/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_FIELD_INTEGER_H
#define MQPP_PACKET_FIELD_INTEGER_H

#include <span>
#include <cstdint>

#include <mqpp/byte_wide.h>

namespace mqpp::packet::field
{
	template<byte_wide T = std::uint8_t> class integer
	{
	public:
		integer(const std::span<T> buffer) noexcept : m_buffer(buffer) {}
		static std::size_t estimate() noexcept {return 2;}
		operator std::int16_t() const noexcept
		{
			return static_cast<std::int16_t>((m_buffer[0] << 8) | m_buffer[1]);
		}

		explicit operator std::uint16_t() const noexcept
		{
			return static_cast<std::uint16_t>((m_buffer[0] << 8) | m_buffer[1]);
		}

		integer& operator=(const std::int16_t v) noexcept
		{
			m_buffer[0] = static_cast<std::uint8_t>(v >> 8);
			m_buffer[1] = static_cast<std::uint8_t>(v & 0xff);

			return *this;
		}

		integer& operator=(const std::uint16_t v) noexcept
		{
			m_buffer[0] = static_cast<std::uint8_t>(v >> 8);
			m_buffer[1] = static_cast<std::uint8_t>(v & 0xff);

			return *this;
		}

		std::size_t size() const noexcept {return 2;}

	private:
		std::span<T> m_buffer;
	};
}

#endif // MQPP_PACKET_FIELD_INTEGER_H
