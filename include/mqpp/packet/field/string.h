/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_FIELD_STRING_H
#define MQPP_PACKET_FIELD_STRING_H

#include <algorithm>
#include <span>
#include <string_view>
#include <cstdint>

#include <mqpp/byte_wide.h>

#include <mqpp/packet/field/integer.h>

namespace mqpp::packet::field
{
	template<byte_wide T = std::uint8_t> class string
	{
	public:
		string(const std::span<T> buffer) noexcept : m_buffer(buffer) {}
		static std::size_t estimate(const std::string_view v) noexcept {return v.size() + 2;}
		operator std::string_view() const noexcept
		{
			const char* const p{reinterpret_cast<const char*>(m_buffer.data() + 2)};
			return std::string_view{p, size() - 2};
		}

		string& operator=(const std::string_view s) noexcept
		{
			std::copy(std::begin(s), std::end(s), std::begin(m_buffer) + 2);
			integer sf{m_buffer};
			sf = static_cast<std::uint16_t>(s.size());

			return *this;
		}

		std::size_t size() const noexcept
		{
			integer sf{m_buffer};
			return static_cast<std::uint16_t>(sf) + 2;
		}

	private:
		std::span<T> m_buffer;
	};
}

#endif // MQPP_PACKET_FIELD_STRING_H
