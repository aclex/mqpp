/*
	mqpp - micro mqtt C++ library
	Copyright (C) 2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
#ifndef MQPP_PACKET_FIELD_PAYLOAD_H
#define MQPP_PACKET_FIELD_PAYLOAD_H

#include <algorithm>
#include <span>
#include <string_view>
#include <cstdint>

#include <mqpp/byte_wide.h>

namespace mqpp::packet::field
{
	template<byte_wide T = std::uint8_t> class payload
	{
	public:
		payload(const std::span<T> buffer) noexcept : m_buffer(buffer) {}
		static std::size_t estimate(const std::span<byte_wide auto> v) noexcept {return v.size();}
		operator std::span<T>() const noexcept {return m_buffer;}
		payload& operator=(const std::span<byte_wide auto> s) noexcept
		{
			std::copy(std::begin(s), std::end(s), std::begin(m_buffer));
			return *this;
		}

		std::size_t size() const noexcept {return m_buffer.size();}

	private:
		std::span<T> m_buffer;
	};
}

#endif // MQPP_PACKET_FIELD_PAYLOAD_H
